const express = require('express')
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

// MIDDLEWARES
app.use(bodyParser.json());
app.use(cors());

// Import Routes
const postsRoute = require('./routes/posts');
app.use('/posts', postsRoute);


// ROUTES - GET = get, PATCH = update, DELETE = delete
app.get('/', (req,res) => {
    res.send('Home page');
})

// Connect to DB
mongoose.connect(
    process.env.DB_CONNECTION, 
    { useNewUrlParser: true }, 
    () => console.log('Connected to DB'));

// LISTENING TO SERVER
app.listen(3000);