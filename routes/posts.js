const express = require('express');
const router = express.Router();
const Post = require('../models/Post');


// Gets back all the posts
router.get('/', async (req, res) => {
    try  {
        const posts = await Post.find();
        res.json(posts);
    } catch(err) {
        res.json({message: err})
    }
})


// SUBMIT POST
router.post('/', async (req, res) => {
    const post = new Post({
        title: req.body.title,
        description: req.body.description
    });
    try {
        const savedPost = await post.save();
        res.json(savedPost);
    }catch(err) {
        res.json({message: err})
    }
});


// GET BACK SPECIFIC POST
router.get('/:postID', async (req, res) => {
    try {
        const post =  await Post.findById(req.params.postID);
        res.json(post);
    } catch (err) {
        res.json({message: err})
    }
    
});

// DELETE POST
router.delete('/:postID', async (req, res) => {
    try {
        const remove = await Post.deleteOne({_id: req.params.postID});
        res.json(remove);
    } catch (err) {
        res.json({message: err})
    }
});

// UPDATE POST
router.patch('/:postID', async (req, res) => {
    try {
        const update = await Post.updateOne(
            {_id: req.params.postID}, 
            {$set: {title: req.body.title}}
        );
        res.json(update);
    } catch (err) {
        res.json({message: err})
    }
})

module.exports = router;
